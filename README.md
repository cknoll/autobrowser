[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

# autobrowser

Simple script for browser automation based on [splinter](https://splinter.readthedocs.io/en/latest/).

Currently this script is taylored to one specific usecase: immediatly booking a 90min video conference. However it might serve as an example on how to build similar applications.

## Installation:

- Run `pip install -e .` in this directory. This installs in "editable mode" which means your changes to the code take immidiate effect.
    - Note: this automatically installs an executable script named `autobrowser`
- Copy `autobrowser.ini` to your home directory

## Usage:

- `autobrowser bbb` → book a video conference
- `autobrowser --help` → display help and other commands


## References:

- Recommended for password handling: https://www.passwordstore.org/
- https://github.com/henriquebastos/python-decouple/ (separation of config from code)
- https://splinter.readthedocs.io/en/latest/ (library for browser automation)

